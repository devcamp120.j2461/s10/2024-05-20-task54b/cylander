public class Cylander extends Circle{
    private double height = 1.0;

    public Cylander() {
    }


    public Cylander(double radius) {
        super(radius);
    }

    public Cylander(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public Cylander(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume() {        
        return this.getArea()*this.height;
    }
}
