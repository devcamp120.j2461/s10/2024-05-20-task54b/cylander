public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.00);
        Circle circle3 = new Circle(3.00,"green");

        System.out.println(circle1);
        System.out.println("Area: " + circle1.getArea());

        System.out.println(circle2);
        System.out.println("Area: " + circle2.getArea());

        System.out.println(circle3);
        System.out.println("Area: " + circle3.getArea());

        Cylander c1 = new Cylander();
        Cylander c2 = new Cylander(2.5);
        Cylander c3 = new Cylander(3.5, 1.5);
        Cylander c4 = new Cylander(3.5, "green", 1.5);

        System.out.println(c1);
        System.out.println("Volume: " + c1.getVolume());
        System.out.println(c2);
        System.out.println("Volume: " + c2.getVolume());
        System.out.println(c3);
        System.out.println("Volume: " + c3.getVolume());
        System.out.println(c4);
        System.out.println("Volume: " + c4.getVolume());
        
    }
}
